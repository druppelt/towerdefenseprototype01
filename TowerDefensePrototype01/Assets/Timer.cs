﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// ReSharper disable InconsistentNaming
public class Timer : MonoBehaviour {

    private int _count = 0;
    private float _timer = 0;
    public Text _timerText;

	// Use this for initialization
	void Start () {
        SetTimerText();
	}
	
	// Update is called once per frame
	void Update () {

        _timer+=Time.deltaTime; //_timer is in seconds
        if (_timer >= 1) //increase _count every second
        {
            _count++;
            SetTimerText();
            _timer -=1;
        }
    }

    void FixedUpdate()
    {
    }

    void SetTimerText()
    {
        _timerText.text = "Time: " + _count.ToString(); 
    }
}
