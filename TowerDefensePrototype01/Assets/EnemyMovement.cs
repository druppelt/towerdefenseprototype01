﻿using UnityEngine;
using System.Collections;
// ReSharper disable InconsistentNaming
public class EnemyMovement : MonoBehaviour {



    private Transform _player;
    private NavMeshAgent _nav;
    private PlayerHealth _playerHealth;
    private EnemyHealth _enemyHealth;

	// Use this for initialization
	void Awake () {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
	    _playerHealth = _player.GetComponent<PlayerHealth>();
	    _enemyHealth = GetComponent<EnemyHealth>();
        _nav = GetComponent<NavMeshAgent>();
	}


	
	// Update is called once per frame
	void Update () {
	    if (_enemyHealth.currentHealth > 0 && _playerHealth.CurrentHealth > 0)
	    {
            _nav.SetDestination(_player.position);
        }
	    else
	    {
	        _nav.enabled = false;
	    }

	}
}
