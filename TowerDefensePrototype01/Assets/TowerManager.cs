﻿using System;
using UnityEngine;
using System.Collections;

public class TowerManager : MonoBehaviour
{
    [Serializable]
    public class BuildItem
    {
        [Tooltip("The Prefab to use. ATM only Turrets.")]
        public GameObject Prefab; 
        [Tooltip("The Name that should be displayed in the Build Manager")]
        public String Name;
        [Tooltip("The Icon that should be displayed in the Build Manager")]
        public Sprite Sprite;
        // Sprite assignment -> this.transform.getComponent<UnityEngine.UI.Image>().sprite = newSprite;
    }


    public GameObject SelectedTower;
    public BuildItem[] BuildItems;

   

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public GameObject Build(GameObject plattform)
    {
        if (plattform == null)
        {
            Debug.LogError("plattform is null");
            return null;
        }
        GameObject tower = null;
        if (SelectedTower != null)
        {
            TowerSpot ts = plattform.GetComponent<TowerSpot>();
            if (ts.Tower != null)
            {
                Debug.Log("TowerManager.Build: Plattform already has a tower");
            }
            else
            {
                tower = (GameObject)Instantiate(SelectedTower, plattform.transform.position, plattform.transform.rotation);
                ts.Tower = tower;
            }
        }
        return tower;
    }
}
