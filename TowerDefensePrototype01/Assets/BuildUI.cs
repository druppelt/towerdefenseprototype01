﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class BuildUI : MonoBehaviour
{
    private float _hoverDistance = 2f;
    private TowerManager _towerManager;
    private GameObject _selectedPlattform;
    private Camera _camera;
    private GameObject _uiStuff;


    // Use this for initialization
    void Start () {
        _towerManager = FindObjectOfType<TowerManager>();
        _camera = GetComponent<Canvas>().worldCamera;
        _uiStuff = transform.GetChild(0).gameObject;
        _uiStuff.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
//        transform.forward = _camera.transform.forward;
    }

    public void ShowUI(GameObject plattform)
    {
        _uiStuff.SetActive(true);
        _selectedPlattform = plattform;
        transform.position = plattform.transform.position + new Vector3(0, _hoverDistance, 0);
    }

    public void HideUI()
    {
        _selectedPlattform = null;
        _uiStuff.SetActive(false);
    }

    public void Build()
    {
        _towerManager.Build(_selectedPlattform);
        HideUI();
    }

}
