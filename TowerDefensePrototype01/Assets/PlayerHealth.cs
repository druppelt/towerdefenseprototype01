﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
// ReSharper disable InconsistentNaming
public class PlayerHealth : MonoBehaviour
{

    public int StartingHealth = 100;
    public int CurrentHealth;
    public Slider HealthSlider;
    public Image DamageImage;
    public float FlashSpeed = 5f;
    public Color FlashColor = new Color(1f, 0f, 0f, 0.2f);
    public DeathController DeathController;

    private bool _damaged;
    private bool _isDead;

    // Use this for initialization
    void Start () {
	
	}

    //nope, i'm using this for init :P
    void Awake()
    {
        CurrentHealth = StartingHealth;
    }
	
	// Update is called once per frame
	void Update () {
	    if (_damaged)
	    {
	        DamageImage.color = FlashColor;
            _damaged = false;
        }
	    else
	    {
	        DamageImage.color = Color.Lerp(DamageImage.color, Color.clear, FlashSpeed*Time.deltaTime);
	    }
	}

    public void TakeDamage(int amount)
    {
        _damaged = true;
        CurrentHealth -= amount;
        HealthSlider.value = CurrentHealth;
        //maybe play audio?
        
        if (CurrentHealth <= 0 && !_isDead)
        {
            Death();
        }
    }

    private void Death()
    {
        _isDead = true;
        DeathController.TriggerDeathScreen();
        //handle game over
        //idk, huge explosion and shit
    }
}
