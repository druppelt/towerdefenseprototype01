﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour
{

    public PlayerHealth PlayerHealth;
    public GameObject Enemy;
    public float SpawnTime = 3f;
    public Transform[] SpawnPoints;

    // Use this for initialization
	void Start () {
        InvokeRepeating("Spawn", SpawnTime, SpawnTime);
	}

    void Spawn()
    {
        if (PlayerHealth.CurrentHealth <= 0)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, SpawnPoints.Length);

        Instantiate(Enemy, SpawnPoints[spawnPointIndex].position, SpawnPoints[spawnPointIndex].rotation);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
