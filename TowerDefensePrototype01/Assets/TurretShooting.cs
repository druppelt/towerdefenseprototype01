﻿using UnityEngine;
using System.Collections;
// ReSharper disable InconsistentNaming

public class TurretShooting : MonoBehaviour
{

    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 3f;

    public GunBarrelEndShooting[] GunBarrelEnds;
    private int _barrelIndex = 0;

    private float _timer;
    //Audio?
//    private float _effectsDisplayTime = 0.2f;

//    private Animator _anim;

    private bool _shoot;



	// Use this for initialization
	void Awake ()
	{
        //	    _anim = GetComponentInChildren<Animator>();

        foreach (var gunBarrelEnd in GunBarrelEnds)
        {
            gunBarrelEnd.initStats(damagePerShot,timeBetweenBullets,range);
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
	    _timer += Time.deltaTime;

	    if (_shoot && _timer >= timeBetweenBullets)
	    {
//	        foreach (var gunBarrelEnd in GunBarrelEnds)
//	        {
//	            gunBarrelEnd.Shoot();
//	        }
            GunBarrelEnds[_barrelIndex].Shoot();
            _barrelIndex++;
            if (_barrelIndex >= GunBarrelEnds.Length)
                _barrelIndex = 0;

            _timer = 0;
            _shoot = false;
        }
	}

    public void Shoot()
    {
        _shoot = true;
    }
}
