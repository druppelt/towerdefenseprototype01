﻿using UnityEngine;
using System.Collections;
// ReSharper disable InconsistentNaming
public class EnemyAttack : MonoBehaviour
{

    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    private Animator _anim;
    private GameObject _player;
    private PlayerHealth _playerHealth;
    private EnemyHealth _enemyHealth;
    private bool _playerInRange;
    private float _timer;

    void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerHealth = _player.GetComponent<PlayerHealth>();
        _enemyHealth = GetComponent<EnemyHealth>();
        _anim = GetComponentInChildren<Animator>();
    }

    // Use this for initialization
    void Start()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == _player)
        {
            _playerInRange = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject == _player)
        {
            _playerInRange = false;
        }
    }


    void Attack()
    {
        if (_playerHealth.CurrentHealth > 0)
        {
            _playerHealth.TakeDamage(attackDamage);
            _anim.SetTrigger("Attack");
        }
    }


    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if (_timer >= timeBetweenAttacks && _playerInRange && _enemyHealth.currentHealth > 0)
        {
            Attack();
            _timer = 0;
        }

        //if (_playerHealth.CurrentHealth <= 0)
        //{
        //    //play death anim for player
        //}
    }
}
