﻿using UnityEngine;
using System.Collections;
// ReSharper disable InconsistentNaming
public class EnemyHealth : MonoBehaviour
{

    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;

    public float sinkDelay = 5f;

    private Animator _anim;
    //death audio 
    //hit particels
    private CapsuleCollider _capsuleCollider;
    private bool _isDead;
    private bool _isSinking;
    private float _timer;

	// Use this for initialization
	void Awake ()
	{
	    _anim = GetComponentInChildren<Animator>();
	    _capsuleCollider = GetComponent<CapsuleCollider>();

	    currentHealth = startingHealth;
	}
	
	// Update is called once per frame
	void Update () {
	    if (_isSinking)
	    {
	        _timer += Time.deltaTime;
	        if (_timer >= sinkDelay)
	        {
	            transform.Translate(-Vector3.up*sinkSpeed*Time.deltaTime);
	        }
	    }
	}

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if (_isDead)
        {
            return;
        }

        //play damage audio
        currentHealth -= amount;

        //put hit particles to hitpoint and play 

        if (currentHealth <= 0)
        {
            Death();
        }
        else
        {
            _anim.SetTrigger("Hit");
        }

    }

    void Death()
    {
        _isDead = true;
        _capsuleCollider.isTrigger = true; //so other rigid bodies can move through the corpes without beeing stuck
        _anim.SetTrigger("Dead");
        //play death audio

        ScoreManager.score += scoreValue;

        //make it not longer shootabel
        gameObject.layer = 0;
        gameObject.tag = "Untagged";
    }

    public void StartSinking()
    {
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true; // to avoid unity recalculating stuff
        _isSinking = true;
        _timer = 0f;
        Destroy(gameObject,2f+sinkDelay); //after 2 secs the object should be gone and can be destroyed
    }

}
