﻿using UnityEngine;
using System.Collections;

public class SinkingProxy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartSinking()
    {
        //The Animator is on the child of the Enemy, thus the Event gets fired on the child and has to be redirected to the parent. 
        //Seems ugly, but idk better -.-
        transform.parent.GetComponent<EnemyHealth>().StartSinking();        
    }
}
