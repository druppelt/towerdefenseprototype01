﻿using UnityEngine;
using System.Collections;
// ReSharper disable InconsistentNaming
public class TurretAutoAim : MonoBehaviour
{

    public float Range = 3f; //The detection and shooting range. Does not determine bullet reach, only gun control

    GameObject _target; //TODO find target by itself and design prio scoring, like nearest, highest dmg, highest health, nearest to dest, most crowded etc
    private TurretShooting _turretShooting;

	// Use this for initialization
	void Awake ()
	{
	    _turretShooting = GetComponentInChildren<TurretShooting>();
	}
	
	// Update is called once per frame
    void Update()
    {
        _target = FindClosestEnemy();
        // TODO  max rotation speed
        if (_target != null)
        {
            Vector3 direction = _target.transform.position - transform.position;
            direction.y = 0f; //can only shoot in a straight line
            if (direction.magnitude <= Range)
            {
                //aim at target and shoot
                Quaternion quaternion = Quaternion.LookRotation(direction);
                transform.rotation = quaternion;
                _turretShooting.Shoot();
            }

            
        }
    }

    GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("ShootableEnemy");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }
}
