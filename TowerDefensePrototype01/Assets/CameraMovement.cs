﻿using UnityEngine;
using System.Collections;
// ReSharper disable InconsistentNaming
public class CameraMovement : MonoBehaviour
{
    private float _camSpeed = 10f;
    private Vector3 _movement;

	// Use this for initialization
	void Start () {
	    _movement = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        MoveCamera(h,v);
    }


    void MoveCamera(float h, float v)
    {
        _movement = new Vector3(h,0f,v);
        _movement = _movement.normalized * _camSpeed * Time.fixedDeltaTime;

        gameObject.transform.Translate(_movement);
    }
}
