﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
// ReSharper disable InconsistentNaming
public class DeathController : MonoBehaviour
{

    public float deathFadeSpeed;
    public float textFadeInOffset;
    public Color deathTextColor;

    private CanvasGroup _deathCanvasGroup;
    private Text _deathText;
    private bool _isDead;
    private float _timer;

    void Awake()
    {
        _deathCanvasGroup = gameObject.GetComponent<CanvasGroup>();
        _deathText = gameObject.GetComponentInChildren<Text>();  
        _deathText.color =  new Color(deathTextColor.r, deathTextColor.g, deathTextColor.b, 0f);
        _deathText.rectTransform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
    }   

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (_isDead)
        {
            _timer += Time.deltaTime;
            _deathCanvasGroup.alpha = Mathf.Lerp(_deathCanvasGroup.alpha, 1f, deathFadeSpeed * Time.deltaTime);
            if (_timer >= textFadeInOffset)
            {
                _deathText.color = Color.Lerp(_deathText.color, deathTextColor, deathFadeSpeed * Time.deltaTime);
                _deathText.rectTransform.localScale = Vector3.Slerp(_deathText.rectTransform.localScale, Vector3.one,
                    deathFadeSpeed*Time.deltaTime);
            }
        }
    }


    public void TriggerDeathScreen()
    {
        _isDead = true;
        _timer = 0;
    }
}
