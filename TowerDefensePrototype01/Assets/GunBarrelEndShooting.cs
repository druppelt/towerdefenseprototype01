﻿using UnityEngine;
using System.Collections;

public class GunBarrelEndShooting : MonoBehaviour {


    private int damagePerShot = 20;
    private float timeBetweenBullets = 0.15f;
    private float range = 3f;

    private float _timer;
    private Ray _shootRay;
    private RaycastHit _shootHit;
    private int _shootableMask;
    private ParticleSystem _gunParticles;
    private LineRenderer _gunLine;
    //Audio?
    private Light _gunLight;
    private float _effectsDisplayTime = 0.2f;

//    private Animator _anim;


    public void initStats(int dps, float tbb, float r)
    {
        damagePerShot = dps;
        timeBetweenBullets = tbb;
        range = r;
    }

    // Use this for initialization
    void Awake () {
        _shootableMask = LayerMask.GetMask("ShootableEnemy");
        _gunParticles = GetComponent<ParticleSystem>();
        _gunLine = GetComponent<LineRenderer>();
        _gunLight = GetComponent<Light>();
//        _anim = transform.parent.GetComponentInChildren<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        _timer += Time.deltaTime;


        if (_timer >= timeBetweenBullets * _effectsDisplayTime)
        {
            DisableEffects();
        }
    }


    private void DisableEffects()
    {
        _gunLight.enabled = false;
        _gunLine.enabled = false;
    }

    public void Shoot()
    {
        _timer = 0f;
//        _anim.SetTrigger("Shoot");

        _gunLight.enabled = true;

        // stop & start to reset if its still playing
        _gunParticles.Stop();
        _gunParticles.Play();

        _gunLine.enabled = true;
        _gunLine.SetPosition(0, transform.position); //set lines start to gunbarrelends pos

        _shootRay.origin = transform.position;
        _shootRay.direction = transform.forward;

        if (Physics.Raycast(_shootRay, out _shootHit, range, _shootableMask))
        {
            EnemyHealth enemyHealth = _shootHit.collider.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, _shootHit.point);
            }
            _gunLine.SetPosition(1, _shootHit.point);
        }
        else
        {
            _gunLine.SetPosition(1, _shootRay.origin + _shootRay.direction * range);
        }

    }
}
